<%@ page contentType="text/html; charset=utf-8" %>
<%@ page import="com.fasterxml.jackson.databind.ObjectMapper"%>
<%
    String json = new ObjectMapper().writeValueAsString(sample.MySample.map);
%>
<SCRIPT type="text/javascript">
    var val = JSON.parse('<%= json %>')

    document.addEventListener('DOMContentLoaded', function(evt){
        document.querySelector('button').addEventListener('click', function(evt){
            console.log('** val:', val)
            console.log('** val["one"]:', val['one'])
            console.log('** val["two"]:', val['two'])
            console.log('** val["three"]:', val['three'])
        })
    })
</SCRIPT>
<html>
<body>
    <button>ホタン</button>
</body>
</html>


<%@ page contentType="text/html; charset=utf-8" %>
<%@ page import="java.util.List"%>
<%@ page import="com.fasterxml.jackson.databind.ObjectMapper"%>
<%
    // List<String> list = sample.MySample.list;
    String json = new ObjectMapper().writeValueAsString(sample.MySample.list);
%>
<SCRIPT type="text/javascript">
    // var val0 = '<%= json %>'
    var val = JSON.parse('<%= json %>')

    document.addEventListener('DOMContentLoaded', function(evt){
        document.querySelector('button').addEventListener('click', function(evt){
            console.log('**tip002: val:', val)
            console.log('**tip002: val[0]:', val[0])
            console.log('**tip002: val[1]:', val[1])
            console.log('**tip002: val[2]:', val[2])
        })
    })
</SCRIPT>
<html>
<body>
    <button>ホタン</button>
</body>
</html>

